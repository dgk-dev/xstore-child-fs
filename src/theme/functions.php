<?php

/** analytics */
// require(get_stylesheet_directory().'/includes/analytics.php');

/** Cambios en tienda */
require(get_stylesheet_directory().'/includes/store-changes.php');

/** Cambios en grid de productos y slider */
// require(get_stylesheet_directory().'/includes/etheme-product.php');
require(get_stylesheet_directory().'/includes/etheme-slider.php');

/** Funciones de chat */
// require(get_stylesheet_directory().'/includes/chat.php');


/** Estilos de login */
require(get_stylesheet_directory().'/includes/login-style.php');

add_action('wp_enqueue_scripts', 'fs_resources');
function fs_resources(){
    etheme_child_styles();
    wp_register_script('fapia-functions', get_stylesheet_directory_uri() . '/js/footer-bundle.js', null, '1.0', true);
	// wp_localize_script('fapia-functions', 'fapiaGlobalObject', array(
	// 	'add_cart_button_script' => is_shop() || is_product_category() || is_product_tag(),
	// ));
    wp_enqueue_script('fapia-functions');
}

//langs: themes and plugins
add_action( 'after_setup_theme', 'fs_theme_lang' );
function fs_theme_lang() {
    load_child_theme_textdomain( 'xstore', get_stylesheet_directory() . '/languages' );
    
    //plugins
    unload_textdomain('xstore-core');
    load_textdomain('xstore-core', get_stylesheet_directory() . '/languages/xstore-core-es_ES.mo');
}

/**
 * Gestión de roles shop_sales = ventas : dado de alta desde plugin
 */
add_action( 'wp_before_admin_bar_render', 'fs_admin_bar_remove_logo', 0 );
function fs_admin_bar_remove_logo() {
    global $wp_admin_bar;
    $wp_admin_bar->remove_menu( 'wp-logo' );
}
add_action( 'admin_menu', 'fs_remove_menu_pages', 999);
function fs_remove_menu_pages() {
  global $current_user;
   
  $user_roles = $current_user->roles;
  
  $roles = array('pedidos', 'vender', 'productos');
  
  if(count(array_intersect($user_roles, $roles))){
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-settings');
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-addons');
    $remove_submenu = remove_submenu_page('woocommerce', 'wc-status');
    $remove_menu = remove_menu_page('woocommerce-marketing');
  }

  $roles = array('blog', 'citas');
  if(count(array_intersect($user_roles, $roles))){
    $remove_menu = remove_menu_page('edit.php?post_type=testimonials');
    $remove_menu = remove_menu_page('edit.php?post_type=staticblocks');
    $remove_menu = remove_menu_page('edit.php?post_type=etheme_portfolio');
    $remove_menu = remove_menu_page('wpcf7');
    $remove_menu = remove_menu_page('tools');
    $remove_menu = remove_menu_page('vc-welcome');
  }
  $roles = array('blog');
  if(count(array_intersect($user_roles, $roles))){
    $remove_menu = remove_menu_page('easy_app_top_level');
  }
}