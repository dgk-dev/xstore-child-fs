<?php
/**
 * Custom login screen
 */
add_action('wp_dashboard_setup', 'fs_dashboard_widgets');
function fs_dashboard_widgets() {
    global $wp_meta_boxes;
    wp_add_dashboard_widget('custom_help_widget', 'Bienvenido', 'custom_dashboard_message');
}
 
function custom_dashboard_message() {
    echo '<div style="padding:20px 0; text-align:center;"><img src="'.get_stylesheet_directory_uri().'/img/fapia-logo-n.svg"></div><p><strong>Este es el administrador del sitio Fapia Shop</strong>';
}

add_action( 'login_enqueue_scripts', 'fs_login_logo' );
function fs_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
            background-image: url('<?php echo get_stylesheet_directory_uri(); ?>/img/fapia-logo-n.svg');
            height: 200px;
            width: 200px;
            background-size: 200px 200px;
            background-repeat: no-repeat;
        	padding-bottom: 1em;
        }
    </style>
<?php }

add_filter( 'login_headerurl', 'fs_loginlogo_url');
function fs_loginlogo_url($url) {
    return home_url();
}