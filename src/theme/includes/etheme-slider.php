<?php
/**
 * Sobreescribir función del slider para agregar filtro de parámetros de WP query, todo el código es igual a excepción del filtro
 */
function etheme_slider( $args, $type = 'post', $atts = array() ) {
    // ! Slider args
    $slider_atts = array(
        'title'              => false,
        'before'             => '',
        'after'              => '',
        'wrapper_class'		 => '',
        'class'              => '',
        'attr'               => '',
        'echo'               => false,
        'large'              => 4,
        'notebook'           => 4,
        'tablet_land'        => 3,
        'tablet_portrait'    => 2,
        'mobile'             => 2,
        'slider_autoplay'    => 'no',
        'slider_speed'       => 300,
        'slider_interval'    => 3000,
        'slider_stop_on_hover' => false,
        'slider_loop'        => false,
        'autoheight'         => true,
        'pagination_type'    => 'hide',
        'nav_color' 		 => '',
        'arrows_bg_color' 	 => '',
        'default_color'      => '#e6e6e6',
        'active_color'       => '#b3a089',
        'hide_fo'            => '',
        'hide_buttons'       => false,
        'navigation_type'    => 'arrow',
        'navigation_position_style' => 'arrows-hover',
        'navigation_style'	 => '',
        'navigation_position'=> 'middle',
        'hide_buttons_for'	 => '',
        'size'               => 'shop_catalog',
        'per_move'           => 1,
        // ! blog args
        'slide_view'         => '',
        'hide_img' => false,
        'blog_align'         => '',
        'blog_hover'		 => 'zoom',
        // ! Products args 
        'block_id'           => false,
        'style'              => 'default',
        'product_view'       => '',
        'product_view_color' => '',
        'no_spacing'         => '',
        'shop_link'          => false,
        'slider_type'        => false,
        'from_first'         => '',
        'widget'             => false,
        'wrap_widget_items_in_div' => false,
        'elementor'		 	 => false,
        'is_preview'		 => false
    );

    extract( shortcode_atts( $slider_atts, $atts ) );

    add_filter('et_view-mode-grid', '__return_true');
    
    // fix for variation galleries
    if ( function_exists('remove_et_variation_gallery_filter'))
        remove_et_variation_gallery_filter('');
    $args = apply_filters( 'etheme_get_products_slider_args', $args, $wrapper_class );
    $box_id      = rand( 1000, 10000 );
    $multislides = new WP_Query( $args );
    $loop = $slide_class = $html = '';

    if ( $slider_stop_on_hover ) 
        $class .= ' stop-on-hover';

    if ( $type == 'post' ) {
        global $et_loop;
        $et_loop['slider']      = true;
        $et_loop['blog_layout'] = 'default';
        $et_loop['size']        = $size;
        $et_loop['hide_img'] = $hide_img;
        $et_loop['blog_hover'] = $blog_hover;
        $et_loop['slide_view']  = $slide_view;
        $et_loop['blog_align']  = $blog_align;
        $class .= ' posts-slider';
    } else {
        if( ! class_exists( 'Woocommerce' ) ) return;
        global $woocommerce_loop;
        if ( !isset($woocommerce_loop['size']) || empty( $woocommerce_loop['size'] ) )
            $woocommerce_loop['size'] = $size;

        if( ! $slider_type ) {
            $woocommerce_loop['lazy-load'] = false;
            $woocommerce_loop['style'] = $style;
        }
        $product_view = etheme_get_option('product_view', 'disable');
        if( !empty($woocommerce_loop['product_view'])) {
            $product_view = $woocommerce_loop['product_view'];
        }

        $custom_template = etheme_get_custom_product_template();
        if( !empty($woocommerce_loop['custom_template'])) {
            $custom_template = $woocommerce_loop['custom_template'];
        }

        $block = '';
        $class .= ' products-slider';
        
        $slide_class .= ' slide-item product-slide ';
        $slide_class .= $slider_type . '-slide';

        if( $no_spacing == 'yes' ) $slide_class .= ' item-no-space';

        if( $block_id && $block_id != '' && etheme_static_block( $block_id, false ) != '' ) {
            ob_start();
                echo '<div class="slide-item '.$slider_type.'-slide">';
                    etheme_static_block($block_id, true);
                echo '</div><!-- slide-item -->';
            $block = ob_get_contents();
            ob_end_clean();
        }
    }

    if ( $multislides->have_posts() ) {
        if ( $type == 'post' ) {
            add_filter( 'excerpt_length', 'etheme_excerpt_length_sliders', 1000 );
        }
        $autoheight = ( $autoheight ) ? 'data-autoheight="1"' : '';
        $lines = ( $pagination_type == 'lines' ) ? 'swiper-pagination-lines' : '';
        $slider_speed = ( $slider_speed ) ? 'data-speed="' . $slider_speed . '"' : '';
        
        if ( $slider_autoplay ) $slider_autoplay = $slider_interval;
        if ( $autoheight ) $autoheight = 'data-autoheight="1"';
        if ( $slider_loop ) $loop = ' data-loop="true"';

        $selectors = array();
        $selectors['slider'] = '.slider-'.$box_id;
        $selectors['pagination'] = $selectors['slider'] . ' .swiper-pagination-bullet';
        $selectors['pagination_active'] = $selectors['pagination'] . '-active,' . $selectors['pagination'] . ':hover';

        $selectors['navigation'] = $selectors['slider'] . ' ~ .swiper-button-prev,' . $selectors['slider'] . ' ~ .swiper-button-next';

        $output_css = array(
            'pagination' => array(),
            'navigation' => array()
        );
    
        $wrapper_class .= ' ' . $navigation_position;
        $wrapper_class .= ' ' . $navigation_position_style;
       
        $html .= '<div class="swiper-entry '.$wrapper_class.'">';
            $html .= $before;

            $html .= ( $title ) ? '<h3 class="title"><span>' . $title . '</span></h3>' : '';

            if ( $type == 'product' && $product_view == 'custom' && $custom_template != '' ) { 
                $class  .= ' products-with-custom-template products-template-'.$custom_template;
                $attr .= ' data-post-id="'.$custom_template.'"';
            }

            $elementor_nospacing = '';
            if ( true == $elementor && 'yes' == $no_spacing ) {
                $elementor_nospacing = 'data-space="0"';
            }

            $html .='
                <div
                    class="swiper-container carousel-area ' . $class . ' slider-' . $box_id . ' ' . $lines . '"
                    '. $elementor_nospacing .'
                    data-breakpoints="1"
                    data-xs-slides="' . esc_js( $mobile ) . '"
                    data-sm-slides="' . esc_js( $tablet_land ) . '"
                    data-md-slides="' . esc_js( $notebook ) . '"
                    data-lt-slides="' . esc_js( $large ) . '"
                    data-slides-per-view="' . esc_js( $large ) . '"
                    ' . $autoheight . '
                    data-slides-per-group="' . esc_attr( $per_move ). '"
                    data-autoplay="' . esc_attr( $slider_autoplay ) . '"
                    ' . $slider_speed . ' ' . $loop . ' ' . $attr . '
                >
            ';

                $html .= '<div class="swiper-wrapper">';
                    $_i=0;
    
                    ob_start();
                    
                    while ( $multislides->have_posts() ) : $multislides->the_post();
                        $_i++;

                        if ( $type == 'product' ) {

                            global $product;

                            if( ( $from_first == 'no' && $_i ==  2) || ( $from_first != 'no' && $_i == 1 ) ) {
                                echo $block; // All data escaped 
                            }
                            
                            if ( ! $product->is_visible() ) continue;

                            if ( $widget ) {
                                    if ( $wrap_widget_items_in_div ) {
                                        $is_widget_slider = true;
                                        echo '<div class="swiper-slide' . esc_attr( $slide_class ) . '">';
                                    }
                                    wc_get_template_part( 'content', 'widget-product-slider' );
                                    if ( $wrap_widget_items_in_div ) {
                                        unset($is_widget_slider);
                                        echo '</div>';
                                    }
                                
                            } else {
                                echo '<div class="swiper-slide' . esc_attr( $slide_class ) . '">';
                                    wc_get_template_part( 'content', 'product-slider' );
                                echo '</div>';
                            }
 
                        } else {
                            echo '<div class="swiper-slide' . esc_attr( $slide_class ) . '">';
                                get_template_part( 'content', 'grid' );
                            echo '</div>';
                        }
                        
                    endwhile;
                
                $html .= ob_get_clean();
                $html .= '</div><!-- slider wrapper-->';

                if ( $pagination_type != 'hide' ) {
                    $pagination_class = '';
                    if ( $hide_fo == 'desktop' ) 
                        $pagination_class = ' dt-hide';
                    elseif ( $hide_fo == 'mobile' ) 
                        $pagination_class = ' mob-hide';

                    $html .= '<div class="swiper-pagination '.$pagination_class.'"></div>';

                    if ( !empty($default_color) ) {
                        $output_css['pagination'][] = $selectors['pagination'] . ' { background-color: ' .$default_color . '; }';
                    }

                    if ( !empty($active_color) ) {
                        $output_css['pagination'][] = $selectors['pagination_active'] . '{ background-color: ' .$active_color . '; }';
                    }

                    if ( count($output_css['pagination']) ) {

                        if ( $is_preview ) {
                            $html .= '<style>' . implode(' ', $output_css['pagination']) . '</style>';
                        }
                        else {
                            wp_add_inline_style( 'xstore-inline-css', implode(' ', $output_css['pagination']) );
                        }
                    }

                }
            $html .= '</div><!-- slider container-->';

            if ( ! $hide_buttons || ( $hide_buttons && $hide_buttons_for != '' ) ) {
                $navigation_class = '';
                if ( $elementor == true ) {
                    $navigation_class .= ' et-swiper-elementor-nav';
                }
                if ( $hide_buttons_for == 'desktop' ) 
                    $navigation_class = ' dt-hide';
                elseif ( $hide_buttons_for == 'mobile' ) 
                    $navigation_class = ' mob-hide';
    
                $navigation_class_left  = 'swiper-custom-left' . ' ' . $navigation_class;
                $navigation_class_right = 'swiper-custom-right' . ' ' . $navigation_class;
    
                $navigation_class_left .= ' type-' . $navigation_type . ' ' . $navigation_style;
                $navigation_class_right .= ' type-' . $navigation_type . ' ' . $navigation_style;
    
                if ( $navigation_position == 'bottom' )
                    $html .= '<div class="swiper-navigation">';
                
                if ( false == $elementor || ( true == $elementor && 'middle' == $navigation_position || 'middle-inside' == $navigation_position ) ) {
                    $html .= '
                    <div class="swiper-button-prev ' . $navigation_class_left . '"></div>
                    <div class="swiper-button-next ' . $navigation_class_right . '"></div>
                    ';
                }
    
                if ( $navigation_position == 'bottom' )
                    $html .= '</div>';

                if ( !empty($arrows_bg_color) ) {
                    $output_css['navigation'][] = 'background-color: ' .$arrows_bg_color; 
                }

                if ( !empty($nav_color) ) {
                    $output_css['navigation'][] = 'color: ' .$nav_color; 
                }

                if ( count($output_css['navigation']) ) {

                    $output_css['navigation'] = $selectors['navigation'] . '{' . implode(';', $output_css['navigation']) . '}';

                    if ( $is_preview ) {
                        $html .= '<style>' . $output_css['navigation'] . '</style>';
                    }
                    else {
                        wp_add_inline_style( 'xstore-inline-css', $output_css['navigation'] );
                    }
                }

                // wp_add_inline_style( 'xstore-inline-css', 
                //     	'.slider-'.$box_id.' .swiper-button-prev, .slider-'.$box_id . ' .swiper-button-next {background-color:'.$arrows_bg_color.'; color: '. $nav_color .';}'
            }

            $html .= $after;
        $html .= '</div><div class="clear"></div><!-- slider-entry -->';
        if ( $type == 'post' ) {
            remove_filter( 'excerpt_length', 'etheme_excerpt_length_sliders', 1000 );
        }
    };
    
    // end fix for variation galleries
    if (function_exists('add_et_variation_gallery_filter'))
        add_et_variation_gallery_filter('');

    remove_filter('et_view-mode-grid', '__return_true');

    if ( $type == 'post' ) {
        unset( $et_loop );
        wp_reset_postdata();
    } else {
        wp_reset_query();
        unset( $woocommerce_loop['lazy-load'] );
        unset( $woocommerce_loop['style'] );
    }

    if ( $is_preview ) 
        $html .= '<script>jQuery(document).ready(function(){ 
                etTheme.swiperFunc();
                etTheme.secondInitSwipers();
                etTheme.global_image_lazy(); 
                etTheme.contentProdImages();
                etTheme.countdown(); 
                etTheme.customCss();
                etTheme.customCssOne();
            });</script>';


    if ( $echo ) {
        echo $html; // All data escaped
    } else {
        return $html;
    }
}

add_filter('etheme_get_products_slider_args', 'fs_recently_purchased_products', 10, 2);
function fs_recently_purchased_products($args, $wrapper_class){
    if(isset($args['posts_per_page']) && $args['posts_per_page'] != 'recently-purchased') return $args;
    
    $orders =  wc_get_orders( array(
        'limit'    => 5,
        'status'   => array('completed'),
        'orderby' => 'date',
        'order' => 'DESC',
    ) );

    $posts_in = array();
    foreach( $orders as $post ) {
    
        $order = new WC_Order($post->get_id());
        $products = $order->get_items(); // get products in current order
    
        // loop through products to find matching id's and add qty when matching current id
        foreach ( $products as $product ) {
            $posts_in[] = $product->get_product_id();
            // do your stuff as you wish here
            
        } // end foreach $product
        
    } // end foreach $post
    add_filter( 'pre_option_woocommerce_hide_out_of_stock_items', function( $option ) { return "no"; }, 10, 1 );
    $args['post__in'] = $posts_in;
    $args['orderby'] = 'post__in';
    $args['posts_per_page'] = count($posts_in);

    return $args;
}