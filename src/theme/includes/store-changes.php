<?php

//mensaje extra en recuperar password
add_action('woocommerce_after_lost_password_confirmation_message', 'fs_after_lost_password_confirmation_message');
function fs_after_lost_password_confirmation_message(){
	echo "<p><strong>Si no lo encuentras en la bandeja de entrada, busca en tu bandeja de spam o correo no deseado.</strong></p>";
}

/**
 * Validar teléfono a 10 dígitos
 */

add_filter( 'woocommerce_checkout_fields', 'fs_remove_default_phone_validation' );
function fs_remove_default_phone_validation( $fields ){
    unset( $fields['billing']['billing_phone']['validate'] );
	return $fields;
}
add_filter( 'woocommerce_billing_fields' , 'fs_custom_override_checkout_fields' );
function fs_custom_override_checkout_fields( $fields ) {
    $fields['billing_phone']['maxlength'] = 10;    
    return $fields;
}
add_action('woocommerce_checkout_process', 'fs_custom_validate_billing_phone');
function fs_custom_validate_billing_phone() {
    $is_correct = preg_match('/^[0-9]{10}$/', $_POST['billing_phone']);
    if ( $_POST['billing_phone'] && !$is_correct) {
        wc_add_notice( __( 'El número de teléfono tiene que ser <strong>de 10 dígitos</strong>.' ), 'error' );
    }
}

/**
 * Reescribe etiquetas de "facturación" a "envío" forzando el envío a la dirección de facturación en woocommerce.
 */
add_filter( 'gettext', 'fs_change_billing_field_strings', 20, 3 );
function fs_change_billing_field_strings( $translated_text, $text, $domain ) {
    switch ( $translated_text ) {
        case 'Detalles de Facturación':
        case 'Dirección de facturación':
        case 'Facturación &amp; Envío':
            $translated_text = __( 'Dirección de envío', 'woocommerce' );
        break;
        
    }
    return $translated_text;
}

add_filter( 'woocommerce_add_error', 'fs_change_wc_errors' );
function fs_change_wc_errors( $error ) {
    if ( strpos( $error, 'Billing ' ) !== false || strpos( $error, 'Facturación ' ) !== false ) {
        $error = str_replace("Billing ", "", $error);
        $error = str_replace("Facturación ", "", $error);
    }
    return $error;
}

//poner imagen en etiqueta de "sale" con soporte para mix-blend
function etheme_woocommerce_sale_flash( $span, $post, $product ) {
	$element_options                   = array();
	$element_options['single_product'] = false;
	$element_options['single_product'] = apply_filters( 'etheme_sale_label_single', $element_options['single_product'] );
	$element_options['in_percentage']  = etheme_get_option( 'sale_percentage', 0 );
	$element_options['in_percentage']  = apply_filters( 'etheme_sale_label_percentage', $element_options['in_percentage'] );
	
	$element_options['is_customize_preview'] = is_customize_preview();
	
	$element_options['sale_icon']       = etheme_get_option( 'sale_icon', 1 );
	$element_options['sale_label_text'] = etheme_get_option( 'sale_icon_text', 'Sale' );
	$element_options['show_label']      = $element_options['sale_icon'] || $element_options['is_customize_preview'];
	
	$element_options['wrapper_class'] = '';
	
	if ( $element_options['single_product'] ) {
		$element_options['sale_label_type']     = etheme_get_option( 'product_sale_label_type_et-desktop', 'square' );
		$element_options['show_label']          = $element_options['sale_label_type'] != 'none' || $element_options['is_customize_preview'];
		$element_options['sale_label_position'] = etheme_get_option( 'product_sale_label_position_et-desktop', 'right' );
		$element_options['sale_label_text']     = etheme_get_option( 'product_sale_label_text_et-desktop', 'Sale' );
		$element_options['sale_label_text']     = ( $element_options['in_percentage'] ) ? etheme_sale_label_percentage_text( $product, $element_options['sale_label_text'] ) : $element_options['sale_label_text'];
		
		$element_options['class'] = 'type-' . $element_options['sale_label_type'];
		$element_options['class'] .= ' ' . $element_options['sale_label_position'];
		$element_options['class'] .= ' single-sale';
	} else {
		$element_options['sale_label_type']     = $element_options['sale_icon'] ? 'square' : 'none';
		$element_options['sale_label_position'] = 'left';
		$element_options['sale_label_text']     = ( $element_options['in_percentage'] ) ? etheme_sale_label_percentage_text( $product, $element_options['sale_label_text'] ) : $element_options['sale_label_text'];
		
		$element_options['class'] = 'type-' . $element_options['sale_label_type'];
		$element_options['class'] .= ' ' . $element_options['sale_label_position'];
	}
	
	if ( $element_options['sale_label_type'] == 'none' && $element_options['is_customize_preview'] ) {
		$element_options['wrapper_class'] .= ' dt-hide mob-hide';
	}
	
	if ( strpos( $element_options['sale_label_text'], '%' ) != false ) {
		$element_options['class'] .= ' with-percentage';
	}
	
	ob_start();
	
	if ( $element_options['show_label'] ) {
		echo '<div class="sale-wrapper ' . esc_attr( $element_options['wrapper_class'] ) . '"><span class="sale-img"></span><span class="onsale ' . esc_attr( $element_options['class'] ) . '">' . $element_options['sale_label_text'] . '</span></div>';
	}
	
	unset( $element_options );
	
	return ob_get_clean();
}

